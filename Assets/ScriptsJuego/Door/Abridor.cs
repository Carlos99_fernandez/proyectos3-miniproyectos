using UnityEngine;

public class Abridor : MonoBehaviour
{
    public Puertas puertas; // Referencia al script de la puerta corredera

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet")) // Aseg�rate de que el disparo tenga el tag "Bullet"
        {
            BreakObject();
        }
    }

    void BreakObject()
    {
        // Aqu� puedes a�adir efectos de rotura, sonidos, etc.
        Destroy(gameObject); // Destruye el objeto

        // Llama al m�todo para abrir las puertas
        if (puertas != null)
        {
            puertas.OpenDoor();
        }
    }
}
