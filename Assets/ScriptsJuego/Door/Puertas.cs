using UnityEngine;

public class Puertas : MonoBehaviour
{
    public GameObject doorLeft;
    public GameObject doorRight;
    public float slidingDistance = 2.0f;
    public float slidingSpeed = 2.0f;
    private Vector3 doorLeftClosedPosition;
    private Vector3 doorRightClosedPosition;
    private AudioSource audioSource; // Referencia al componente AudioSource

    void Start()
    {
        doorLeftClosedPosition = doorLeft.transform.position;
        doorRightClosedPosition = doorRight.transform.position;
        audioSource = GetComponent<AudioSource>(); // Obt�n el componente AudioSource
    }

    public void OpenDoor()
    {
        // Mueve las puertas a sus posiciones abiertas
        doorLeft.transform.position = Vector3.Lerp(doorLeft.transform.position, doorLeftClosedPosition - transform.right * slidingDistance, Time.deltaTime * slidingSpeed);
        doorRight.transform.position = Vector3.Lerp(doorRight.transform.position, doorRightClosedPosition + transform.right * slidingDistance, Time.deltaTime * slidingSpeed);

        // Reproduce el sonido de apertura si no se est� reproduciendo ya
        if (!audioSource.isPlaying)
        {
            audioSource.Play();
        }
    }

    // El resto del script permanece igual
}
