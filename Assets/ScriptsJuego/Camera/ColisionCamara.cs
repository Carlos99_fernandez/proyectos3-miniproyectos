using UnityEngine;

public class ColisionCamara : MonoBehaviour
{
    public Transform playerBody;
    public float minDistance = 1.0f;
    public float maxDistance = 4.0f;
    public float smooth = 10.0f;

    private Vector3 dollyDir;
    private float distance;

    // Asume que "Player" es el nombre del layer original del jugador.
    private int playerLayer;
    private int ignoreRaycastLayer;

    void Start()
    {
        dollyDir = transform.localPosition.normalized;
        distance = transform.localPosition.magnitude;

        // Guarda el layer original del jugador
        playerLayer = playerBody.gameObject.layer;
        // El layer "Ignore Raycast" por defecto tiene el �ndice 2
        ignoreRaycastLayer = LayerMask.NameToLayer("Ignore Raycast");
    }

    void Update()
    {
        // Cambia temporalmente el layer del jugador para ignorar el raycast
        playerBody.gameObject.layer = ignoreRaycastLayer;

        Vector3 desiredCameraPos = playerBody.TransformPoint(dollyDir * maxDistance);
        RaycastHit hit;

        if (Physics.Linecast(playerBody.position, desiredCameraPos, out hit))
        {
            distance = Mathf.Clamp((hit.distance * 0.9f), minDistance, maxDistance);
        }
        else
        {
            distance = maxDistance;
        }

        // Revierte el layer del jugador a su valor original
        playerBody.gameObject.layer = playerLayer;

        transform.localPosition = Vector3.Lerp(transform.localPosition, dollyDir * distance, Time.deltaTime * smooth);
    }
}
