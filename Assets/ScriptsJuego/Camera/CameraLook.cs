using System.Collections;
using UnityEngine;

public class CameraLook : MonoBehaviour
{
    public float mouseSensitivity = 80f;
    public Transform playerBody;
    public Camera cam; // Referencia a la c�mara

    public float zoomFOV = 30f; // Campo de visi�n cuando est� haciendo zoom
    private float normalFOV; // Campo de visi�n normal
    public float zoomSpeed = 5f; // Velocidad de zoom

    float xRotation = 0f;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        normalFOV = cam.fieldOfView; // Guarda el campo de visi�n inicial de la c�mara
    }

    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up * mouseX);

        // Zoom gradual cuando se presiona el bot�n izquierdo del rat�n
        if (Input.GetMouseButton(1)) // Se usa GetMouseButton para el zoom gradual
        {
            cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, zoomFOV, Time.deltaTime * zoomSpeed);
        }
        else // Vuelve al FOV normal cuando se suelta el bot�n
        {
            cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, normalFOV, Time.deltaTime * zoomSpeed);
        }
    }
}
