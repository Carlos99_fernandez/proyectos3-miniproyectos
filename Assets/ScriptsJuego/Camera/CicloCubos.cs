using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CicloCubos : MonoBehaviour
{
    private Vector3 posicionInicial;
    public float tiempoParaReiniciar = 5f; // Tiempo en segundos para reiniciar la posici�n del cubo.
    private float tiempoTranscurrido;

    void Start()
    {
        // Guarda la posici�n inicial del cubo para poder volver a ella.
        posicionInicial = transform.position;
    }

    void Update()
    {
        // Incrementa el contador de tiempo basado en el tiempo que ha pasado desde el �ltimo frame.
        tiempoTranscurrido += Time.deltaTime;

        // Si el tiempo transcurrido es mayor que el tiempo establecido para reiniciar...
        if (tiempoTranscurrido >= tiempoParaReiniciar)
        {
            // ... entonces resetea la posici�n del cubo a su posici�n inicial.
            transform.position = posicionInicial;
            // Opcionalmente, resetea tambi�n la velocidad para evitar efectos no deseados.
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            // Resetea el contador de tiempo.
            tiempoTranscurrido = 0;
        }
    }
}
