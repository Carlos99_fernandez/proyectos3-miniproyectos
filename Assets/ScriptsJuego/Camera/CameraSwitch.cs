using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitch : MonoBehaviour
{
    public Camera firstPersonCamera;

    public Camera thirdPersonCamera;

    private bool firstPersonEnable = true;

    public Transform[] weaponsTransformThirdPerson;

    public GameObject[] weapons;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            firstPersonEnable = !firstPersonEnable;
            ChangeCamera();
        }
    }

    public void ChangeCamera()
    {
        if (firstPersonEnable)
        {
            firstPersonCamera.enabled = true;
            thirdPersonCamera.enabled = false;
        }
        else
        {
            
             firstPersonCamera.enabled = false;
             thirdPersonCamera.enabled = true;
             ChangeWeaponsThirdPerson();


        }
    }

    public void ChangeWeaponsThirdPerson()
    {

        for (int i =0; i < weapons.Length; i++)
        {
            weapons[i].transform.position = weaponsTransformThirdPerson[i].transform.position;
            weapons[i].transform.rotation = weaponsTransformThirdPerson[i].transform.rotation;
            weapons[i].transform.localScale = weaponsTransformThirdPerson[i].transform.localScale;
        }


    }
}
