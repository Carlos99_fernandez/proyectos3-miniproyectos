using UnityEngine;

public class RecogerObjeto: MonoBehaviour
{
    private AudioSource audioSource;

    void Start()
    {
        // Obt�n el componente AudioSource del objeto
        audioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other)
    {
        // Verifica si el objeto que entr� en el trigger es el jugador
        if (other.CompareTag("Player"))
        {
            // Reproduce el sonido
            audioSource.Play();

            // Aqu� puedes agregar m�s l�gica, como desactivar el objeto recogido
            gameObject.SetActive(false);
        }
    }
}
