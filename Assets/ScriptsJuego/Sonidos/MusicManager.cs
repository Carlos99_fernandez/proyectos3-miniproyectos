using UnityEngine;

public class MusicManager : MonoBehaviour
{
    private static MusicManager instance = null;
    public AudioSource audioSource; // Aseg�rate de asignar este componente en el inspector de Unity.

    public static MusicManager Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            // Si ya existe una instancia diferente de este objeto, destr�yelo.
            Destroy(this.gameObject);
            return;
        }
        else
        {
            // Esta es la primera instancia - hazla la Singleton.
            instance = this;
        }

        // Haz este objeto persistente.
        DontDestroyOnLoad(this.gameObject);

        if (!audioSource)
        {
            audioSource = GetComponent<AudioSource>(); // Intenta obtener el componente AudioSource si no se ha asignado.
        }
    }

    public void PlayMusic()
    {
        if (!audioSource.isPlaying)
        {
            audioSource.Play();
        }
    }

    public void StopMusic()
    {
        if (audioSource.isPlaying)
        {
            audioSource.Stop();
        }
    }

    // Otras funciones relacionadas con la m�sica pueden ir aqu�
}
