using UnityEngine;

public class EnemyContact : MonoBehaviour
{
    public int damageOnContact = 10;
    public GameObject particleSystemPrefab;
    public AudioClip explosionSound;
    private bool canPlayEffects = true; // Controla si los efectos pueden ser reproducidos

    private void OnCollisionEnter(Collision collision)
    {
        // Verifica si el enemigo golpea al jugador y si se pueden reproducir los efectos
        if (collision.gameObject.CompareTag("Player") && canPlayEffects)
        {
            ShowEffects();
            GameManager.Instance.LoseHealth(damageOnContact);
            // Opcional: Destruir el enemigo aqu� si es necesario
             Destroy(gameObject);
        }
        else if (collision.gameObject.CompareTag("Bullet")) // Asume que tu bala tiene la etiqueta "Bullet"
        {
            // Cuando el enemigo es golpeado por una bala, deshabilita los efectos
            canPlayEffects = false;
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && canPlayEffects)
        {
            ShowEffects();
            GameManager.Instance.LoseHealth(damageOnContact);
            // Opcional: Destruir el enemigo aqu� si es necesario
            Destroy(gameObject);
        }
        else if (other.gameObject.CompareTag("Bullet")) // Asume que tu bala tiene la etiqueta "Bullet"
        {
            // Cuando el enemigo es golpeado por una bala, deshabilita los efectos
            canPlayEffects = false;
            Destroy(gameObject);
        }
    }

    void ShowEffects()
    {
        if (canPlayEffects) // Solo muestra los efectos si canPlayEffects es true
        {
            if (particleSystemPrefab)
            {
                GameObject particleSystem = Instantiate(particleSystemPrefab, transform.position, Quaternion.identity);
                Destroy(particleSystem, particleSystem.GetComponent<ParticleSystem>().main.duration);
            }

            if (explosionSound)
            {
                AudioSource.PlayClipAtPoint(explosionSound, transform.position);
            }
        }
    }
}
