using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnPlayer : MonoBehaviour
{
    public Transform respawnPoint; // Punto de respawn
    public Transform posicionPlayer; // Punto de respawn

    public void DeadPlayer()
    {
        posicionPlayer.position = respawnPoint.position;
    }   
}
