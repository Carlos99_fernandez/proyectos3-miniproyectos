using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneOnTouch : MonoBehaviour
{
    // Nombre de la escena a cargar
    public string sceneToLoad;

    private void OnTriggerEnter(Collider other)
    {
        // Verifica si el objeto que toc� el trigger tiene la etiqueta "Player"
        if (other.CompareTag("Player"))
        {
            // Cambia a la escena deseada
            SceneManager.LoadScene(sceneToLoad);
        }
    }
}
