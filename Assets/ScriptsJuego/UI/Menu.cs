using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public GameObject pausePanel;
    public GameObject Mirilla; // Agrega esta l�nea si no la has agregado antes

    public static bool isGamePaused = false;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePause();
        }
    }

    // Cambia el estado de pausa del juego
    public void TogglePause()
    {
        isGamePaused = !isGamePaused;
        if (isGamePaused)
        {
            PauseGame();
        }
        else
        {
            ResumeGame();
        }
    }

    // Pausa el juego
    public void PauseGame()
    {
        Time.timeScale = 0f;
        pausePanel.SetActive(true);
        // Muestra el cursor
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        AudioListener.pause = true; // Detiene los sonidos del juego
        Mirilla.SetActive(false); // Oculta la mirilla
    }

    // Reanuda el juego
    public void ResumeGame()
    {
        Time.timeScale = 1f;
        pausePanel.SetActive(false);
        // Oculta el cursor
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        AudioListener.pause = false; // Reanuda los sonidos del juego
        Mirilla.SetActive(true); // Muestra la mirilla
    }
}
