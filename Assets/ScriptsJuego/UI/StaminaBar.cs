using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaBar : MonoBehaviour
{

    public Slider staminaSlider;

    public float maxStamina = 100;

    private float currentStamina;

    private float regenerateStaminaTime = 0.2f;

    private float regenerateAmount = 2;

    private float losingStaminaTime = 0.01f;

    private Coroutine mycCoroutineLosing;
    private Coroutine mycCoroutineRegenerate;






    void Start()
    {
        currentStamina = maxStamina;
        staminaSlider.maxValue = maxStamina;
        staminaSlider.value = maxStamina;

    }

    public void UseStamina(float amount)
    {
        if (currentStamina-amount > 0)
        {
            if (mycCoroutineLosing != null)
            {
                StopCoroutine(mycCoroutineLosing);
            }

            mycCoroutineLosing = StartCoroutine(LosingStaminaCoroutine(amount));

            if (mycCoroutineRegenerate != null)
            {
                StopCoroutine(mycCoroutineRegenerate);
            }

            mycCoroutineRegenerate = StartCoroutine(RegenerateStaminaCoroutine());
        }
        else
        {
            FindObjectOfType<PlayerMovement>().isSprinting = false;
        }
    }

    private IEnumerator LosingStaminaCoroutine(float amount)
    {
        while (currentStamina >= 0)
        {
            currentStamina -= amount;

            staminaSlider.value = currentStamina;

            yield return new WaitForSeconds(losingStaminaTime);
        }

        mycCoroutineLosing = null;

        FindObjectOfType<PlayerMovement>().isSprinting = false;
    }

    private IEnumerator RegenerateStaminaCoroutine()
    {
        yield return new WaitForSeconds(1);

        while (currentStamina<maxStamina)
        {
            currentStamina += regenerateAmount;

            staminaSlider.value = currentStamina;

            yield return new WaitForSeconds(regenerateStaminaTime);
        }
        mycCoroutineRegenerate = null;
    }
}
