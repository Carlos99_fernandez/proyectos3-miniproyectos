using UnityEngine;

public class PlatformController : MonoBehaviour
{
    public Transform pointA;
    public Transform pointB;
    public float speed = 2.0f;

    private Vector3 nextPosition;

    void Start()
    {
        nextPosition = pointA.position; // Inicializa el siguiente punto al que se mover� la plataforma.
    }

    void Update()
    {
        MovePlatform();
    }

    void MovePlatform()
    {
        if (transform.position == pointA.position)
        {
            nextPosition = pointB.position;
        }
        else if (transform.position == pointB.position)
        {
            nextPosition = pointA.position;
        }

        transform.position = Vector3.MoveTowards(transform.position, nextPosition, speed * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.transform.SetParent(transform);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.transform.SetParent(null);
        }
    }

}
