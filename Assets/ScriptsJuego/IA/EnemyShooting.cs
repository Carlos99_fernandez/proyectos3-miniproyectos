using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
    public GameObject enemyBullet;
    public Transform spawnBulletPoint;
    private Transform playerPosition;
    public float bulletVelocity = 100;
    public float shootingRange = 50; // Define el alcance m�ximo de disparo
    public LayerMask playerLayer; // Capa del jugador para el raycast

    // A�adido: Componentes para el sonido del disparo
    public AudioSource audioSource;
    public AudioClip shootSound;

    void Start()
    {
        playerPosition = FindObjectOfType<PlayerMovement>().transform; // Aseg�rate de que este componente sea el correcto para tu jugador
        Invoke("TryShootPlayer", Random.Range(0f, 10f)); // Invoca TryShootPlayer por primera vez con un tiempo aleatorio entre 0 y 7 segundos
    }

    void Update()
    {
        // Actualizaci�n no necesaria para este script espec�fico
    }

    void TryShootPlayer()
    {
        if (PlayerInSight())
        {
            ShootPlayer();
        }

        // Invoca TryShootPlayer nuevamente con un tiempo aleatorio entre 0 y 7 segundos
        Invoke("TryShootPlayer", Random.Range(0f, 10f));
    }

    void ShootPlayer()
    {
        Vector3 playerDirection = playerPosition.position - transform.position;

        // Reproduce el sonido del disparo
        audioSource.PlayOneShot(shootSound);

        GameObject newBullet = Instantiate(enemyBullet, spawnBulletPoint.position, Quaternion.LookRotation(playerDirection));
        newBullet.GetComponent<Rigidbody>().AddForce(playerDirection.normalized * bulletVelocity, ForceMode.Force);

        Destroy(newBullet, 1); // Aumenta el tiempo antes de destruir la bala
    }

    bool PlayerInSight()
    {
        RaycastHit hit;
        Vector3 directionToPlayer = (playerPosition.position - spawnBulletPoint.position).normalized;

        if (Physics.Raycast(spawnBulletPoint.position, directionToPlayer, out hit, shootingRange, playerLayer))
        {
            if (hit.collider.transform == playerPosition)
            {
                return true; // El jugador est� en la l�nea de visi�n y dentro del rango de disparo
            }
        }
        return false; // El jugador no est� en la l�nea de visi�n o est� fuera del rango de disparo
    }
}
