using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Transform[] destinations;
    private int i = 0;
    public bool followPlayer;
    public float distanceToFollowPath = 2;
    private GameObject player;
    private float distanceToPlayer;
    public float distanceToFollowPlayer = 10;

    // Variables para el audio de movimiento
    public AudioSource movementAudioSource;
    public AudioClip movementClip;
    private bool isMoving = false; // Bandera para controlar el estado del movimiento

    void Start()
    {
        navMeshAgent.destination = destinations[i].transform.position;
        player = FindObjectOfType<PlayerMovement>().gameObject;

        // Configuración inicial del AudioSource
        if (movementAudioSource != null)
        {
            movementAudioSource.clip = movementClip;
            movementAudioSource.loop = true; // Para que el clip se repita mientras el enemigo se mueva
        }
    }

    void Update()
    {
        distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);

        if (distanceToPlayer <= distanceToFollowPlayer && followPlayer)
        {
            FollowPlayer();
        }
        else
        {
            EnemyPath();
        }

        // Control de reproducción del sonido de movimiento
        if (!isMoving && navMeshAgent.velocity.magnitude > 0.1f) // Inicia el sonido si el enemigo comienza a moverse
        {
            movementAudioSource.Play();
            isMoving = true;
        }
        else if (isMoving && navMeshAgent.velocity.magnitude <= 0.1f) // Detiene el sonido si el enemigo se detiene
        {
            movementAudioSource.Stop();
            isMoving = false;
        }
    }

    public void EnemyPath()
    {
        navMeshAgent.destination = destinations[i].position;

        if (Vector3.Distance(transform.position, destinations[i].position) <= distanceToFollowPath)
        {
            if (i != destinations.Length - 1)
            {
                i++;
            }
            else
            {
                i = 0;
            }
        }
    }

    public void FollowPlayer()
    {
        navMeshAgent.destination = player.transform.position;
    }

    public void GrenadeImpact()
    {
        Destroy(gameObject);
    }
}
