using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public TextMeshProUGUI ammoText;
    public static GameManager Instance { get; private set; }

    public int gunAmmo = 10;

    public TextMeshProUGUI healtText;

    public int health = 100;

    public int maxHealth = 100;

    public TextMeshProUGUI messageText; // Referencia al UI Text
    public Image imageToShow; // Referencia al UI Image

    void Start()
    {
        // Verificar si los objetos est�n asignados y activos
        if (messageText == null)
        {
            Debug.LogError("messageText no est� asignado en el inspector.");
        }
        else
        {
            Debug.Log("messageText asignado correctamente.");
        }

        if (imageToShow == null)
        {
            Debug.LogError("imageToShow no est� asignado en el inspector.");
        }
        else
        {
            Debug.Log("imageToShow asignado correctamente.");
        }

        if (messageText.gameObject.activeSelf)
        {
            Debug.Log("messageText est� activo al inicio.");
        }
        else
        {
            Debug.LogWarning("messageText no est� activo al inicio.");
        }

        if (imageToShow.gameObject.activeSelf)
        {
            Debug.Log("imageToShow est� activo al inicio.");
        }
        else
        {
            Debug.LogWarning("imageToShow no est� activo al inicio.");
        }

        // Configura tu mensaje aqu�
        Debug.Log("Starting coroutines to hide message and image...");
        StartCoroutine(HideMessageAfterTime(1)); // Llama al IEnumerator con un retardo de 1 segundo
        StartCoroutine(ShowImageForSeconds(1)); // Llama al IEnumerator para mostrar la imagen por 1 segundo
    }

    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        ammoText.text = gunAmmo.ToString();
        healtText.text = health.ToString();
    }

    public void LoseHealth(int healthToReduce)
    {
        health -= healthToReduce;
        CheckHealth();
    }

    public void CheckHealth()
    {
        if (health <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void AddHealth(int health)
    {
        if (this.health + health >= maxHealth)
        {
            this.health = 100;
        }
        else
        {
            this.health += health;
        }
    }

    IEnumerator HideMessageAfterTime(float time)
    {
        Debug.Log($"Hiding message after {time} seconds...");
        yield return new WaitForSeconds(time);
        if (messageText != null)
        {
            messageText.gameObject.SetActive(false); // Oculta el mensaje
            Debug.Log("Message hidden.");
        }
        else
        {
            Debug.LogError("messageText es null.");
        }
    }

    IEnumerator ShowImageForSeconds(float time)
    {
        Debug.Log($"Hiding image after {time} seconds...");
        yield return new WaitForSeconds(time);
        if (imageToShow != null)
        {
            imageToShow.gameObject.SetActive(false); // Oculta la imagen
            Debug.Log("Image hidden.");
        }
        else
        {
            Debug.LogError("imageToShow es null.");
        }
    }
}
