using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthObject : MonoBehaviour
{
    public int health = 5; // La cantidad de vida que se a�adir� al jugador
    public GameObject particleEffectPrefab; // Prefab del sistema de part�culas

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) // Verifica si el objeto que colisiona es el jugador
        {          
            // Instanciar el efecto de part�culas
            InstantiateParticleEffect();

            // Destruir el objeto de recogida
            Destroy(gameObject);
        }
    }

    private void InstantiateParticleEffect()
    {
        if (particleEffectPrefab != null)
        {
            Instantiate(particleEffectPrefab, transform.position, Quaternion.identity);
        }
    }
}
