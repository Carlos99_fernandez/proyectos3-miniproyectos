using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBox : MonoBehaviour
{
    public int ammo = 10; // Cantidad de munición que proporciona la caja
    public GameObject particleEffectPrefab; // Prefab del sistema de partículas
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) // Verifica si el objeto que colisiona es el jugador
        {
            // Instanciar el efecto de partículas
            InstantiateParticleEffect();

            // Destruir el objeto de recogida
            Destroy(gameObject);
        }
    }

    private void InstantiateParticleEffect()
    {
        if (particleEffectPrefab != null)
        {
            Instantiate(particleEffectPrefab, transform.position, Quaternion.identity);
        }
    }

}
