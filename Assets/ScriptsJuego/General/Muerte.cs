using UnityEngine;

public class Muerte: MonoBehaviour
{
    void Start()
    {
        // Hacer el cursor visible
        Cursor.visible = true;

        // Permitir que el cursor se mueva libremente
        Cursor.lockState = CursorLockMode.None;
    }
}
