using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController characterController;
    public float speed = 10f;
    private float gravity = -9.81f;
    public Transform groundCheck;
    public float sphereRadius = 0.3f;
    public LayerMask groundMask;
    bool IsGrounded;
    Vector3 velocity;
    public float jumpHeight = 3;
    public bool isSprinting;
    public float sprintingSpeedMultiplier = 1.5f;
    private float sprintSpeed = 1;
    public float staminaUseAmount = 2f; // Ajusta la velocidad de consumo de estamina
    public float staminaRecoveryRate = 3f; // Ajusta la velocidad de recuperaci�n de estamina
    private Slider staminaSlider; // Cambiado de StaminaBar a Slider
    public Animator animator;

    // Variable para el AudioSource y el �nico AudioClip para pasos
    public AudioSource footstepSound;
    public AudioClip footstepClip; // Ahora solo necesitas un AudioClip

    private void Start()
    {
        staminaSlider = FindObjectOfType<Slider>(); // Encuentra el Slider en la escena
        animator = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        IsGrounded = Physics.CheckSphere(groundCheck.position, sphereRadius, groundMask);

        if (IsGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        animator.SetFloat("VelX", x);
        animator.SetFloat("VelZ", z);
        animator.SetBool("isSprinting", isSprinting);

        Vector3 move = transform.right * x + transform.forward * z;

        CheckJump();
        StartCoroutine(RunCheck());

        // Modificado para reproducir un �nico sonido de pasos
        if (IsGrounded && move.magnitude > 0 && !footstepSound.isPlaying)
        {
            footstepSound.clip = footstepClip;
            footstepSound.loop = true; // Aseg�rate de que el sonido se repita
            footstepSound.Play();
        }
        else if (!IsGrounded || move.magnitude == 0)
        {
            footstepSound.loop = false; // Detiene la repetici�n
            footstepSound.Stop(); // Detiene el sonido si el jugador no se est� moviendo o est� en el aire
        }

        characterController.Move(move * speed * Time.deltaTime * sprintSpeed);

        velocity.y += gravity * Time.deltaTime;

        characterController.Move(velocity * Time.deltaTime);
    }

    public void CheckJump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && IsGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
            animator.SetBool("isJumping", true);
        }

        if (!IsGrounded)
        {
            animator.SetBool("isJumping", false);
        }
    }

    private IEnumerator RunCheck()
    {
        while (Input.GetKey(KeyCode.LeftShift))
        {
            isSprinting = true;
            sprintSpeed = sprintingSpeedMultiplier;

            staminaSlider.value -= staminaUseAmount * Time.deltaTime;
            yield return null;
        }

        isSprinting = false;
        sprintSpeed = 1;

        // Recuperaci�n de estamina acelerada
        while (!Input.GetKey(KeyCode.LeftShift))
        {
            staminaSlider.value += staminaRecoveryRate * Time.deltaTime;
            yield return null;
        }
    }
}
