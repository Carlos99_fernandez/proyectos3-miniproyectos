using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteractions : MonoBehaviour
{

    public Transform respawnPoint;
    public Transform respawnPointObjeto;
    public Transform respawnPointSueloFinal;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("GunAmmo"))
        {
            GameManager.Instance.gunAmmo += other.gameObject.GetComponent<AmmoBox>().ammo;
            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("HealthObject"))
        {
            GameManager.Instance.AddHealth(other.gameObject.GetComponent<HealthObject>().health);
            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("DeathZone"))
        {
            GameManager.Instance.LoseHealth(10);

            GetComponent<CharacterController>().enabled = false;
            gameObject.transform.position = respawnPoint.position;
            GetComponent<CharacterController>().enabled = true;


        }
        if (other.gameObject.CompareTag("ObjetoMuerte"))
        {
            GameManager.Instance.LoseHealth(10);

            GetComponent<CharacterController>().enabled = false;
            gameObject.transform.position = respawnPointObjeto.position;
            GetComponent<CharacterController>().enabled = true;


        }

        if (other.gameObject.CompareTag("SueloFinal"))
        {
            GameManager.Instance.LoseHealth(10);

            GetComponent<CharacterController>().enabled = false;
            gameObject.transform.position = respawnPointSueloFinal.position;
            GetComponent<CharacterController>().enabled = true;


        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("EnemyBullet"))
        {
            GameManager.Instance.LoseHealth(5);
        }
        else if (collision.gameObject.CompareTag("Enemy"))
        {
            GameManager.Instance.LoseHealth(35);
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("DamageCube")) 
        {
            GameManager.Instance.LoseHealth(101);

            Destroy(collision.gameObject);
        }
    }

}
