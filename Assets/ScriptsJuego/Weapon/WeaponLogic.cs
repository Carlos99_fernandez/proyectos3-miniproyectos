using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponLogic : MonoBehaviour
{
    public Transform spawnPoint;
    public GameObject bulletPrefab;
    public Camera mainCamera;
    public float shotForce = 1500f;
    public float shotRate = 0.5f;

    private float nextShootTime = 0f;
    private AudioSource audioSource;
    public AudioClip shotGun;
    public bool continuousShooting = false;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && !Menu.isGamePaused) // Aseg�rate de tener una referencia a Menu.isGamePaused
        {
            if (Time.time >= nextShootTime && GameManager.Instance.gunAmmo > 0)
            {
                if (continuousShooting)
                {
                    InvokeRepeating("Shoot", 0f, shotRate);
                }
                else
                {
                    Shoot();
                }
            }
        }
        else if (Input.GetKeyUp(KeyCode.Mouse0) && continuousShooting)
        {
            CancelInvoke("Shoot");
        }
    }

    void Shoot()
    {
        if (GameManager.Instance.gunAmmo > 0)
        {
            if (audioSource != null)
            {
                audioSource.PlayOneShot(shotGun);
            }

            GameManager.Instance.gunAmmo--;

            // Obt�n la posici�n del cursor en el mundo
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Vector3 shootDirection = Vector3.zero;

            if (Physics.Raycast(ray, out hit))
            {
                shootDirection = (hit.point - spawnPoint.position).normalized;
            }
            else
            {
                // Si no se golpea nada, dispara en la direcci�n de la c�mara
                shootDirection = mainCamera.transform.forward;
            }

            GameObject newBullet = Instantiate(bulletPrefab, spawnPoint.position, Quaternion.identity);

            // Aplica la fuerza en la direcci�n calculada
            newBullet.GetComponent<Rigidbody>().AddForce(shootDirection * shotForce);

            // Establece el tiempo para el pr�ximo disparo
            nextShootTime = Time.time + shotRate;

            // Destruye la bala despu�s de un tiempo
            Destroy(newBullet, 3);
        }
        else
        {
            CancelInvoke("Shoot");
        }
    }
}
