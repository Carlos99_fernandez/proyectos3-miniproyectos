using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour
{
    public float delay = 2;

    float countdown;

    public float radius = 5;

    public float explosionForce = 70;

    bool exploded = false;

    public GameObject explosionEffect;


    private AudioSource audiosSource;

    public AudioClip explosion;

    void Start()
    {
        countdown = delay;
        audiosSource = GetComponent<AudioSource>();
    }


    void Update()
    {
        countdown -= Time.deltaTime;

        if (countdown <= 0 && exploded == false)
        {
            Explode();
            exploded = true;    

        }
    }

    void Explode()
    {

        Instantiate(explosionEffect, transform.position, transform.rotation);

        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);

        foreach (var rangeObject in colliders)
        {
            AI aI = rangeObject.GetComponent<AI>();

            if (aI != null)
            {
                aI.GrenadeImpact();
            }

            Rigidbody rb = rangeObject.GetComponent<Rigidbody>();

            if (rb != null)
            {
                rb.AddExplosionForce(explosionForce * 10, transform.position, radius);
            }
        }

        audiosSource.PlayOneShot(explosion);

        gameObject.GetComponent<SphereCollider>().enabled = false;
        gameObject.GetComponent<MeshRenderer>().enabled = false;

        Destroy(gameObject, delay*2);
    }
}
