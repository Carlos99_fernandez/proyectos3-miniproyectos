using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float bulletLifetime = 2.5f; // Tiempo de vida de la bala si no choca con nada

    void Start()
    {
        // Destruir la bala despu�s de un tiempo especificado
        Destroy(gameObject, bulletLifetime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Verificar si la colisi�n es con un enemigo
        if (collision.gameObject.CompareTag("Enemy"))
        {
            // Destruir el enemigo
            Destroy(collision.gameObject);
        }

        // Destruir la bala al chocar con cualquier objeto
        Destroy(gameObject);
    }
}
