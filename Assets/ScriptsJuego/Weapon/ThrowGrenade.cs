using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowGrenade : MonoBehaviour
{
    public float throwForce = 500;
    public GameObject grenadePrefab;

    void Update()
    {
        // Se verifica si la tecla asignada para lanzar granadas ha sido presionada
        // y simultáneamente se consulta el estado de pausa del juego.
        if (Input.GetKeyDown(KeyCode.E) && !Menu.isGamePaused) // Asume la existencia de GameManager.isGamePaused
        {
            Throw();
        }
    }

    public void Throw()
    {
        // Creación de la instancia de la granada y aplicación de la fuerza de lanzamiento.
        GameObject newGrenade = Instantiate(grenadePrefab, transform.position, transform.rotation);
        newGrenade.GetComponent<Rigidbody>().AddForce(transform.forward * throwForce);
    }
}
