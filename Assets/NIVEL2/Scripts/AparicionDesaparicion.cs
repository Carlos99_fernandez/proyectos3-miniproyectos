using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AparicionDesaparicion: MonoBehaviour
{
    public GameObject[] bloques; // Array para almacenar los bloques
    public float intervalo = 8f;

    private void Start()
    {
        StartCoroutine(ToggleBloques());
    }

    IEnumerator ToggleBloques()
    {
        while (true)
        {
            yield return new WaitForSeconds(intervalo);

            // Desactiva o activa cada bloque en el array
            foreach (GameObject bloque in bloques)
            {
                bloque.SetActive(!bloque.activeSelf);
            }
        }
    }
}
