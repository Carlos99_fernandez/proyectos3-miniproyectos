using UnityEngine;

public class Desaparecer: MonoBehaviour
{
    // Este es el tag que deber�s asignar al jugador o al cubo que al tocar har� desaparecer este objeto.
    public GameObject objectToDestroy;

    private void OnTriggerEnter(Collider other)
    {
        // Verifica si el objeto con el que colisionamos es el jugador.
        // Aseg�rate de que tu jugador tenga asignado el tag "Player" en el inspector de Unity.
        if (other.gameObject.CompareTag("Player"))
        {
            // Destruye el cubo.
            Destroy(objectToDestroy);

            // Destruye la esfera (este objeto).
            Destroy(gameObject);
        }
    }
}
