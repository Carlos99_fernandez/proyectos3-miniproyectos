using UnityEngine;

public class MovimientoBloques: MonoBehaviour
{
    public Transform puntoInicio;
    public Transform puntoFin;
    public float velocidad = 1.0f;
    private float factor = 0;

    void Update()
    {
        // Mover el objeto de ida y vuelta entre puntoInicio y puntoFin
        factor += velocidad * Time.deltaTime;
        transform.position = Vector3.Lerp(puntoInicio.position, puntoFin.position, Mathf.PingPong(factor, 1));
    }
}
