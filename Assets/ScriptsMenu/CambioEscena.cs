using UnityEngine;
using UnityEngine.SceneManagement;

public class CambioEscena : MonoBehaviour
{
    public AudioClip buttonSound; // Referencia al clip de sonido del bot�n

    public void PlayButtonSound()
    {
        // Reproduce el sonido del bot�n
        AudioSource.PlayClipAtPoint(buttonSound, Camera.main.transform.position, 1f); // Aseg�rate de que el volumen es adecuado para tus necesidades
    }

    public void NuevaEscena()
    {
        PlayButtonSound();
        Time.timeScale = 1;
        AudioListener.pause = false;
        SceneManager.LoadScene("InicioJuego");
        Menu.isGamePaused = false;
        MusicManager.Instance.PlayMusic();

    }

    public void Controles()
    {
        PlayButtonSound();
        SceneManager.LoadScene("Controles");
    }

    public void Volver()
    {
        
        SceneManager.LoadScene("Menu");
        MusicManager.Instance.PlayMusic();
    }

    public void SalirJuego()
    {
        PlayButtonSound();
        Application.Quit();
    }

    public void VolverNivel1()
    {
        PlayButtonSound();
        SceneManager.LoadScene("Nivel1");
    }

    public void MenuPrincipal()
    {
        PlayButtonSound();
        SceneManager.LoadScene("Menu");
    }

    public void NivelTutorial()
    {
        PlayButtonSound();
        SceneManager.LoadScene("NivelTutorial");
    }
}
