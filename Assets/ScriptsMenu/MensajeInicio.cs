using System.Collections;
using UnityEngine;
using UnityEngine.UI; // Importante para trabajar con UI Text

public class MensajeInicio: MonoBehaviour
{
    public Text messageText; // Referencia al UI Text

    void Start()
    {
        messageText.text = "�Nivel iniciado!"; // Configura tu mensaje aqu�
        StartCoroutine(HideMessageAfterTime(3)); // Llama al IEnumerator con un retardo de 3 segundos
    }

    IEnumerator HideMessageAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        messageText.gameObject.SetActive(false); // Oculta el mensaje
    }
}